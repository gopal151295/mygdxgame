package com.gopal151295.mygdxgame;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MyGdxGame extends ApplicationAdapter {

    SpriteBatch batch;
    Texture ball, ground;
    float currentVelocity = 0f;
    float gravity = 1f;
    float ballY;
    int gameState = 0;

	@Override
	public void create () {
        batch = new SpriteBatch();
        ball = new Texture("ball.png");
        ground = new Texture("ground.png");
        ballY = Gdx.graphics.getHeight()/2- ball.getHeight()/2;
	}

	@Override
	public void render () {
        if (gameState == 0){
            if (Gdx.input.justTouched()){
                gameState = 1;
            }

        }else if (gameState == 1){

            if (Gdx.input.justTouched()){
                currentVelocity = -20;
            }

            if (ballY > 0 || currentVelocity < -10){
                currentVelocity = currentVelocity + gravity;
                ballY = ballY - (currentVelocity );
            }

        }

        batch.begin();
        batch.draw(ground, 0,0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        batch.draw(ball, Gdx.graphics.getWidth()/2 - ball.getWidth()/2, ballY);
        batch.end();

	}

	@Override
	public void dispose () {
        ground.dispose();
        batch.dispose();

	}
}
